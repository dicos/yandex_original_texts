# README #

Этот микропроект был создан для решения задачи https://www.fl.ru/projects/2242527/python_django--originalnyie-tekstyi-yandeks.html

## Установка ##
1.выполнить команду pip install -r requirements.pip

2. выполнить команду python manage.py makemigrations

3. выполнить команду python manage.py migrate

4. запустить тесовый веб-сервер, войти по адресу localhost:8000

5. В левом верхнем углу нажать на кнопку "Добавить текст"

6. Добавить текст

7. После добавление нажать на кнопку "Отправить текст"


##Боевая установка##
1. Чтобы начать пользоваться, нужно зарегистрировать приложение на https://oauth.yandex.ru/client/new При регистрации выбрать права доступа "Яндекс.Вебмастер->Добавлять сайты в сервис Яндекс.Вебмастер и получать информацию о статусе индексирования" и callback url поставить в значение http://localhost:8000/complete/yandex-oauth2/

2. Открыть файл yandex_original_texts/settings.py и изменить значения SOCIAL_AUTH_YANDEX_OAUTH2_KEY (ID) и SOCIAL_AUTH_YANDEX_OAUTH2_SECRET (Пароль)

3. Зарегистрировать сайт в webmaster.yandex.ru