#coding: utf8
from django.db import models


class Page(models.Model):
    ''' example page  '''
    name = models.CharField('Name', max_length=50)
    slug = models.SlugField(unique=True)
    text = models.TextField('Text')

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.name
