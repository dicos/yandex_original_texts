#coding: utf8
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

from .forms import PageForm
from .models import Page
from .utils import get_site_info_url, get_site_id, make_url, send_original_text


@login_required
def texts_list(request):
    ''' slists of texts '''
    data = {'pages': Page.objects.all()}
    return render(request, 'yandex/texts_list.html', data)


@login_required
def text_info(request, slug):
    ''' slists of texts '''
    obj = Page.objects.get(slug=slug)
    data = {'form': PageForm(request.POST or None, instance=obj),
            'is_save': True,
            'page': obj}
    if data['form'].is_valid():
        data['form'].save()
    return render(request, 'yandex/text_info.html', data)


@login_required
def text_add(request):
    ''' slists of texts '''
    data = {'form': PageForm(request.POST or None)}
    if data['form'].is_valid():
        obj = data['form'].save()
        return redirect('text_info', slug=obj.slug)
    return render(request, 'yandex/text_info.html', data)


@login_required
def text_send(request, slug):
    ''' slists of texts '''
    social_user = request.user.social_auth.all()[0]
    s_info_url = get_site_info_url(social_user.uid,
                                   social_user.access_token,
                                   'dicos.ru')
    site_id = get_site_id(s_info_url, social_user.access_token)
    text_url = make_url(site_id)
    page = Page.objects.get(slug=slug)
    send_original_text(text_url, social_user.access_token, page.text)
    return redirect('text_info', slug=page.slug)
