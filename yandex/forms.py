#coding: utf8
from django import forms

from .models import Page


class PageForm(forms.ModelForm):
    def clean_text(self):
        text = self.cleaned_data.get('text')
        if text and not (500 <= len(text) < 32000):
            raise forms.ValidationError('Длина текста должна быть от 500 до 32000 знаков')
        return text

    class Meta:
        model = Page
