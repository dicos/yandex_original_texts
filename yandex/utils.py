#coding: utf8
import re
try:
    from urllib.parse.urlparse import quote
except ImportError:
    from urllib import quote
import xml.etree.ElementTree as ET

import requests

def get_site_info_url(user_id, token, host_name):
    ''' Получаем URL для запроса информации о сайте,
    см https://tech.yandex.ru/webmaster/doc/dg/reference/hosts-docpage/ '''
    url = 'https://webmaster.yandex.ru/api/%s/hosts' % user_id
    headers = {'Authorization': 'OAuth %s' % token}
    root = ET.fromstring(requests.get(url, headers=headers).text)
    for host in root.findall('./host'):
        if host.findall('./name')[0].text == host_name:
            return host.get('href')


def get_original_text_url(url, token):
    ''' Получение url для отправки оригинальных текстов, см
        https://tech.yandex.ru/webmaster/doc/dg/reference/hosts-id-docpage/

        Функция работает некорректно, возможно из-за проблемы на стороне яндекса.
        комментариев от тех.поддержки не получил '''
    headers = {'Authorization': 'OAuth %s' % token}
    root = ET.fromstring(requests.get(url, headers=headers).text)
    return root.findall('./link[@rel=original-texts]')[0].get('href')


def get_site_id(url, token):
    ''' Получение url для отправки оригинальных текстов, см
        https://tech.yandex.ru/webmaster/doc/dg/reference/hosts-id-docpage/

        Функция работает некорректно, возможно из-за проблемы на стороне яндекса.
        комментариев от тех.поддержки не получил '''
    headers = {'Authorization': 'OAuth %s' % token}
    root = ET.fromstring(requests.get(url, headers=headers).text)
    href = root.findall('./link')[0].get('href')
    return re.findall(r'\d+', href)[-1]


def make_url(site_id):
    ''' Собирает url для отправки оригинальных текстов '''
    return 'https://webmaster.yandex.ru/api/v2/hosts/%s/original-texts' % site_id


def send_original_text(url, token, text):
    ''' Посылает оригинальный текст в яндекс, см
        https://tech.yandex.ru/webmaster/doc/dg/reference/host-original-texts-add-docpage/ 
        Если произошла ошибка -- бросается исключение HTTPError '''
    data = '<original-text><content>%s</content></original-text>' % quote(text.encode('utf8'), '')
    headers = {'Authorization': 'OAuth %s' % token}
    resp = requests.post(url, headers=headers, data=data)
    resp.raise_for_status()
