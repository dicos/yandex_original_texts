#coding: utf8
from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'yandex_original_texts.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', 'yandex.views.texts_list', name='texts_list'),
    url(r'^add/$', 'yandex.views.text_add', name='text_add'),
    url(r'^info/(?P<slug>.+)/$', 'yandex.views.text_info', name='text_info'),
    url(r'^text_send/(?P<slug>.+)/$', 'yandex.views.text_send', name='text_send'),

    url(r'^admin/', include(admin.site.urls)),
    url('', include('social.apps.django_app.urls', namespace='social'))
)
